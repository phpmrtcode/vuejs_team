 


// I.1 Cú pháp template
// # văn bản
/*
Hình thức ràng buộc dữ liệu cơ bản nhất là nội suy văn bản (text interpolation) 
sử dụng cú pháp “mustache” (“ria mép” – hai dấu ngoặc nhọn):
*/

// # html thuần túy
/*
Cú pháp mustache sẽ thông dịch dữ liệu ra thành văn bản thuần túy (plain text), 
nghĩa là các kí tự HTML đặc biệt như <>&"' sẽ được mã hóa.
Để xuất ra HTML thuần túy, bạn sẽ cần đến directive v-html.
*/

var obj = {message: 'hello!', msg : 'welcome to VueJs class!', htmlRender: '<button>click</button>', id: 'cant', number: 5, nonumber: 0}
var vm = new Vue({	
  el: '#app',
  data: obj
})


// II. Đối tượng Vue
// II.1 Dữ liệu trong vuejs 
/**
Khi một đối tượng Vue được khởi tạo, 
tất cả các thuộc tính (property) được tìm thấy trong object data sẽ được thêm vào reactivity system 
(hiểu nôm na là “hệ thống phản ứng”) của Vue. 
Điều này có nghĩa là view sẽ “react” (phản ứng) khi giá trị của các thuộc tính này thay đổi, 
và tự cập nhật tương ứng với các giá trị mới.
*/

var data = {a:1, b:2}

var vm = new Vue({
    //el: '#app',
    data: data
});

// vm.a = 4;
// data.b = 1;

console.log('vm.a: ' + vm.a);
console.log('date.a: ' +  data.a);

console.log('vm.b: ' + vm.b);
console.log('date.b: ' +  data.b);